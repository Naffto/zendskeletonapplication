<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Activity\Controller\Activity' => 'Activity\Controller\ActivityController',
        ),
    ),

    'router' => array(
        'routes' => array(
            'activity' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/activity[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            'activity' => __DIR__ . '/../view',
        ),
    ),
);