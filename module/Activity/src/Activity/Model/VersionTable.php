<?php
namespace Activity\Model;

use Zend\Db\TableGateway\TableGateway;

class VersionTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function fetchActive()
    {
        $resultSet = $this->tableGateway->select(/*array('active' => 1)*/);
        return $resultSet;
    }

    public function getVersion($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


}