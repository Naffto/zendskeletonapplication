<?php
namespace Activity\Model;

class ActivityType
{
    public $intId;
    public $strShort;

    public function exchangeArray($data)
    {
        $this->intId     = (isset($data['id'])) ? $data['id'] : null;
        $this->strShort = (isset($data['short'])) ? $data['short'] : null;
    }
}