<?php
namespace Activity\Model;

use Zend\Db\TableGateway\TableGateway;

class ActivityVersionTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function fetchByVersionId($id)
    {
        $id  = (int) $id;
        $resultSet = $this->tableGateway->select(array('version_id' => $id));
        return $resultSet;
    }

    public function fetchByVersionIdActivityId($intVersionId,$intActivityId)
    {
        $intVersionId  = (int) $intVersionId;
        $intActivityId  = (int) $intActivityId;
        $rowset = $this->tableGateway->select(array('version_id' => $intVersionId, 'activity_id' => $intActivityId));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }



    public function getActivityVersion($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


}