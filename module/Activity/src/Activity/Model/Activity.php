<?php
namespace Activity\Model;

class Activity
{
    public $id;
    public $activity_id;
    public $name;
    public $identifier;

    public function exchangeArray($data)
    {
        $this->id     = (isset($data['id'])) ? $data['id'] : null;
        $this->activity_id = (isset($data['activity_id'])) ? $data['activity_id'] : null;
        $this->name  = (isset($data['name'])) ? $data['name'] : null;
        $this->identifier  = (isset($data['identifier'])) ? $data['identifier'] : null;
    }
}