<?php
namespace Activity\Model;

class ActivityVersion
{
    public $intId;
    public $intActivity_id;
    public $intVersion_id;
    public $intActivity_type_id;
    public $floatBudget_limit;
    public $intPosition = 0;
    public $intPlan_Start;
    public $intPlan_Duration;

    public function exchangeArray($data)
    {
        $this->intId     = (isset($data['id'])) ? $data['id'] : null;
        $this->intActivity_id = (isset($data['activity_id'])) ? $data['activity_id'] : null;
        $this->intVersion_id = (isset($data['version_id'])) ? $data['version_id'] : null;
        $this->intActivity_type_id = (isset($data['activity_type_id'])) ? $data['activity_type_id'] : null;
        $this->floatBudget_limit = (isset($data['budget_limit'])) ? $data['budget_limit'] : null;
        $this->intPosition  = (isset($data['position'])) ? $data['position'] : null;
        $this->intPlan_Start  = (isset($data['plan_start'])) ? $data['plan_start'] : null;
        $this->intPlan_Duration  = (isset($data['plan_duration'])) ? $data['plan_duration'] : null;
    }
}