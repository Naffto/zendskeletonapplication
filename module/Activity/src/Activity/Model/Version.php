<?php
namespace Activity\Model;

class Version
{
    public $intId;
    public $boolActive;

    public function exchangeArray($data)
    {
        $this->intId     = (isset($data['id'])) ? $data['id'] : null;
        $this->boolActive = (isset($data['active'])) ? $data['active'] : null;
    }
}