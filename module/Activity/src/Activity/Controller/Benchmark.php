<?php

    namespace Activity\Controller;

    class Benchmark
    {
        private $arrInformation;
        public function __construct() { $this->arrInformation = array(); }
        public function start($strKey) { $this->arrInformation[$strKey]['start'] = microtime(true); }
        public function stop($strKey) { $this->arrInformation[$strKey]['stop'] = microtime(true); }
        public function getTime($strKey) { return number_format($this->arrInformation[$strKey]['stop']-$this->arrInformation[$strKey]['start'], 5, '.', '\'').'s'; }

        public function __toString() {
            $strReturn = '<table id="benchmark">';            
            foreach ($this->arrInformation as $strKey=>$arrSS) {
                if (!isset($arrSS['stop']))
                    $arrSS['stop'] = microtime(true);
                
                if ($arrSS['start'] > 0 && $arrSS['stop'] > 0) {
                    $strReturn .= '<tr><td class="key"><b>'.$strKey.'</b></td><td>'.number_format($arrSS['stop']-$arrSS['start'], 5, '.', '\'').'s</td></tr>';
                }
            }            
            $strReturn .= '<tr><td class="key"><b>mempeak:</b></td><td>'.round((memory_get_peak_usage()/(1024*1024)),3).'mb</td></tr>';
            return $strReturn.'</table>';
        }
    }