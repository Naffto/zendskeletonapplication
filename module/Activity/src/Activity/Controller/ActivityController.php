<?php
namespace Activity\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ActivityController extends AbstractActionController
{
    protected $activityTable = null;
    protected $activityVersionTable = null;
    protected $versionTable = null;

    public function indexAction()
    {
        $str = '';

        //include_once('Benchmark.php');
        $objBenchmark = new Benchmark();
        $objBenchmark->start('activity');

        // $intStarttime = microtime(true);
        foreach($this->getVersionTable()->fetchActive() as $objVersion)
        {
            foreach($this->getActivityVersionTable()->fetchByVersionId($objVersion->intId) as $objActivityVersion)
            {
                #$objActivity = $this->getActivity($objActivityVersion->intActivity_id);
                $arr = $this->executeTreeTraversal($objActivityVersion);

                //var_dump($arr);
                //$str .=  ($arr['start'].','.$arr['end'])."#";
                $str .= $arr['start'].$arr['end'];
                //die;
            }
            //break;
        }
        /*
        die( md5($str) );
        $intEndtime = microtime(true);

        echo ($intEndtime-$intStarttime);
        die();
        */
        $objBenchmark->stop('activity');

        echo $objBenchmark;
        echo md5($str);
        exit();
/*
        return new ViewModel(array(
                            'versions' => $this->getVersionTable()->fetchAll(),
        ));
*/
    }

    protected function getActivity($intActivityId)
    {
        die('no');
        return $this->getActivityTable()->getActivity($intActivityId);
    }

    protected function executeTreeTraversal($objActivityVersion)
    {
        $arrReturn = array('start' => PHP_INT_MAX, 'end' => 0);
        $objActivity = $this->getActivityTable()->getActivity( $objActivityVersion->intActivity_id );

        $arrChildren = $this->getActivityTable()->fetchChild( $objActivity->id );

        if (count($arrChildren) == 0)
        {
            $intStart = (int) $objActivityVersion->intPlan_Start;
            $arrReturn = array('start' => $intStart, 'end' => ($intStart + $objActivityVersion->intPlan_Duration));
        }

        foreach( $arrChildren AS $objActivityChild )
        {
            $objChildActivityVersion = $this->getActivityVersionTable()->fetchByVersionIdActivityId( $objActivityVersion->intVersion_id, $objActivityChild->id);
            $arrTmpReturn = $this->executeTreeTraversal( $objChildActivityVersion );

            $arrReturn['start'] = $arrReturn['start'] < $arrTmpReturn['start'] ? $arrReturn['start'] : $arrTmpReturn['start'];
            $arrReturn['end'] = $arrReturn['end'] >  $arrTmpReturn['end'] ? $arrReturn['end'] :  $arrTmpReturn['end'];

        }

        return $arrReturn;
    }

    public function addAction()
    {
    }

    public function editAction()
    {
    }

    public function deleteAction()
    {
    }

    public function getVersionTable()
    {
        if (!$this->versionTable) {
            $sm = $this->getServiceLocator();
            $this->versionTable = $sm->get('Activity\Model\VersionTable');
        }
        return $this->versionTable;
    }

    public function getActivityTable()
    {
        if (!$this->activityTable) {
            $sm = $this->getServiceLocator();
            $this->activityTable = $sm->get('Activity\Model\ActivityTable');
        }
        return $this->activityTable;
    }

    public function getActivityVersionTable()
    {
        if (!$this->activityVersionTable) {
            $sm = $this->getServiceLocator();
            $this->activityVersionTable = $sm->get('Activity\Model\ActivityVersionTable');
        }
        return $this->activityVersionTable;
    }
}