<?php
/**
 * __PROJECT_NAME__
 *
 * Module.php
 *
 * @category    
 * @package     
 * @subpackage  
 * @copyright   Copyright (c) 2013 Secondred Newmedia GmbH (http://www.secondred.de)
 * @created     17.12.13, 12:59
 * @author      Christian Schultze <schultze@secondred.de>, HoppC
 * @author      $Id$
 */

namespace Activity;

// Add these import statements:
use Activity\Model\Activity;
use Activity\Model\ActivityTable;

use Activity\Model\ActivityVersion;
use Activity\Model\ActivityVersionTable;

use Activity\Model\Version;
use Activity\Model\VersionTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;


class Module
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    // Add this method:
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Activity\Model\ActivityTable' =>  function($sm) {
                        $tableGateway = $sm->get('ActivityTableGateway');
                        $table = new ActivityTable($tableGateway);
                        return $table;
                    },
                'ActivityTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $resultSetPrototype->setArrayObjectPrototype(new Activity());
                        return new TableGateway('activity', $dbAdapter, null, $resultSetPrototype);
                    },

                'Activity\Model\VersionTable' =>  function($sm) {
                        $tableGateway = $sm->get('VersionTableGateway');
                        $table = new VersionTable($tableGateway);
                        return $table;
                    },
                'VersionTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $resultSetPrototype->setArrayObjectPrototype(new Version());
                        return new TableGateway('version', $dbAdapter, null, $resultSetPrototype);
                    },

                'Activity\Model\ActivityVersionTable' =>  function($sm) {
                        $tableGateway = $sm->get('ActivityVersionTableGateway');
                        $table = new ActivityVersionTable($tableGateway);
                        return $table;
                    },
                'ActivityVersionTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $resultSetPrototype->setArrayObjectPrototype(new ActivityVersion());
                        return new TableGateway('activity_version', $dbAdapter, null, $resultSetPrototype);
                    },

            ),
        );
    }

}