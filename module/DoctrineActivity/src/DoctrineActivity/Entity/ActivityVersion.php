<?php

namespace DoctrineActivity\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\Common\Collections\ArrayCollection;

/**
 * 
 * @ORM\Entity 
 * @ORM\Table(name="activity_version")
 */
class ActivityVersion
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Activity", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="activity_id", referencedColumnName="id")
     */
    protected $activity;
    
    /**
     * @ORM\ManyToOne(targetEntity="ActivityType", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="activity_type_id", referencedColumnName="id")
     */
    protected $activity_type;
    
    /**
     * @ORM\ManyToOne(targetEntity="Version", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="version_id", referencedColumnName="id")
     */
    protected $version;
    
    /**
     * @ORM\Column(type="float", scale=15, precision=2)
     */
    protected $budget_limit;
    
    /**
     * @ORM\Column(type="integer", scale=10)
     */
    protected $position;

    /**
     * @ORM\Column(type="smallint", scale=10)
     */
    protected $plan_start;
    
    /**
     * @ORM\Column(type="smallint", scale=10)
     */
    protected $plan_duration;
    
    
    public function __construct()
    {
        $this->childrenActivities = new ArrayCollection();
    }
    
    
    public function getId()
    {
        return $this->id;
    }

    
    public function getActivity()
    {
        return $this->activity;
    }

    public function setActivity($value)
    {
        $this->activity = $value;
    }
    
    public function getActivityType()
    {
        return $this->activity_type;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function getBudgetLimit()
    {
        return $this->budget_limit;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function getPlanStart()
    {
        return $this->plan_start;
    }

    public function getPlanDuration()
    {
        return $this->plan_duration;
    }


}
