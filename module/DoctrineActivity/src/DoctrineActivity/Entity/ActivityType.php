<?php

namespace DoctrineActivity\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * 
 * @ORM\Entity 
 * @ORM\Table(name="activity_type")
 */
class ActivityType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string") 
     */
    protected $short;
    
    public function getId()
    {
        return $this->id;
    }

    public function getShort()
    {
        return $this->short;
    }

    public function setShort($value)
    {
        $this->short = $value;
    }
}
