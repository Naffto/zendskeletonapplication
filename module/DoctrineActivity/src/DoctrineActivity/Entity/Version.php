<?php

namespace DoctrineActivity\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * 
 * @ORM\Entity 
 * @ORM\Table(name="version")
 */
class Version
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="boolean") 
     */
    protected $active;
    
    public function getId()
    {
        return $this->id;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($value)
    {
        $this->active = $value;
    }
}
