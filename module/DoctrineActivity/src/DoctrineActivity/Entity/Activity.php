<?php

namespace DoctrineActivity\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\Common\Collections\ArrayCollection;

/**
 * 
 * @ORM\Entity 
 * @ORM\Table(name="activity")
 */
class Activity
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string") 
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Activity", inversedBy="childrenActivities", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="activity_id", referencedColumnName="id")
     */
    protected $parentActivity;
    
    /**
     * @ORM\OneToMany(targetEntity="Activity", mappedBy="parentActivity", fetch="EXTRA_LAZY")
     */
    protected $childrenActivities;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $identifier;

    
    public function __construct()
    {
        $this->childrenActivities = new ArrayCollection();
    }
    
    
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
    }
    
    public function getParentActivity()
    {
        return $this->parentActivity;
    }

    public function setParentActivity($value)
    {
        $this->parentActivity = $value;
    }
    
    public function getChildrenActivities()
    {
        return $this->childrenActivities;
    }

    public function setChildrenActivity($value)
    {
        $this->childrenActivities = $value;
    }
    
    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setIdentifier($value)
    {
        $this->identifier = $value;
    }
}
