<?php
namespace DoctrineActivity\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class DoctrineActivityController extends AbstractActionController
{
    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    protected $_objectManager;

    public function indexAction()
    {
        include_once('Benchmark.php');
        $objBenchmark = new Benchmark();
        $objBenchmark->start('activity');
        
        $str = '';
        foreach($this->getObjectManager()->getRepository('\DoctrineActivity\Entity\Version')->findAll() as $objVersion)
        {
            foreach($this->getObjectManager()->getRepository('\DoctrineActivity\Entity\ActivityVersion')->findBy(array('version' => $objVersion->getId())) as $objActivityVersion)
            {
                $arr = $this->executeTreeTraversal($objActivityVersion);

                //var_dump($arr);
                //$str .=  ($arr['start'].','.$arr['end'])."#";
                $str .= $arr['start'].$arr['end'];
                //die;
            }
            //break;
        }

        $objBenchmark->stop('activity');

        echo $objBenchmark;
        echo md5($str);
        exit();
        return new ViewModel();
    }
    

    protected function executeTreeTraversal($objActivityVersion)
    {
        $arrReturn = array('start' => PHP_INT_MAX, 'end' => 0);
        $objActivity = $objActivityVersion->getActivity();
        
        
        $arrChildren = $objActivity->getChildrenActivities();

        if (count($arrChildren) == 0)
        {
            
            $intStart = (int) $objActivityVersion->getPlanStart();
            $arrReturn = array('start' => $intStart, 'end' => ($intStart + $objActivityVersion->getPlanDuration()));
        }

        foreach( $arrChildren AS $objActivityChild )
        {
            $objChildActivityVersion = $this->getObjectManager()->getRepository('\DoctrineActivity\Entity\ActivityVersion')->findOneBy(array('version' => $objActivityVersion->getVersion()->getId(), 'activity' => $objActivityChild->getId()));
            $arrTmpReturn = $this->executeTreeTraversal( $objChildActivityVersion );

            $arrReturn['start'] = $arrReturn['start'] < $arrTmpReturn['start'] ? $arrReturn['start'] : $arrTmpReturn['start'];
            $arrReturn['end'] = $arrReturn['end'] >  $arrTmpReturn['end'] ? $arrReturn['end'] :  $arrTmpReturn['end'];

        }
        
        return $arrReturn;
    }
    
    
    /**
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getObjectManager()
    {
        if (!$this->_objectManager) {
            $this->_objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }

        return $this->_objectManager;
    }
}