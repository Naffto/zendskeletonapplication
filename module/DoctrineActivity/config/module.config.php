<?php

namespace DoctrineActivity;

return array(
    'controllers' => array(
        'invokables' => array(
            'DoctrineActivity\Controller\DoctrineActivity' => 'DoctrineActivity\Controller\DoctrineActivityController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'doctrineactivity' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/doctrineactivity',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'DoctrineActivity\Controller\DoctrineActivity',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'doctrineactivity' => __DIR__ . '/../view',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        ),
        'configuration' => array(
            'orm_default' => array(
                'metadata_cache' => 'my_memcache',
                'query_cache' => 'my_memcache',
                'result_cache' => 'my_memcache',
                'hydration_cache' => 'my_memcache',
            )
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'doctrine.cache.my_memcache' => function ($sm)
            {
                $cache = new \Doctrine\Common\Cache\MemcachedCache();
                $memcached = new \Memcached();
                $memcached->addServer('localhost', 11211);
                $cache->setMemcached($memcached);
                return $cache;
            },
        ),
    ),
);
