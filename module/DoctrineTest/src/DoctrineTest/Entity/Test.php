<?php

namespace DoctrineTest\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * 
 * @ORM\Entity 
 * @ORM\Table(name="test")
 *  
 */
class Test
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @property string $name
     * @property int $id
     */
    protected $id;

    /** @ORM\Column(type="string") */
    protected $name;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
    }

}
