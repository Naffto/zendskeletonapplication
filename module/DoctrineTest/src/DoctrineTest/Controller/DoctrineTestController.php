<?php
namespace DoctrineTest\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use \Zend\View\Model\JsonModel;

class DoctrineTestController extends AbstractActionController
{
    protected $_objectManager;

    public function indexAction()
    {
        $memcached = new \Memcached();
        $memcached->addServer('localhost', 11211);

        $cacheDriver = new \Doctrine\Common\Cache\MemcachedCache();
        $cacheDriver->setMemcached($memcached);
        $config = new \Doctrine\ORM\Configuration();
        $config->setQueryCacheImpl($cacheDriver);
        $config->setResultCacheImpl($cacheDriver);
        $config->setMetadataCacheImpl($cacheDriver);
        
        
        $test = $this->getObjectManager()->getRepository('\DoctrineTest\Entity\Test')->findAll();

        return new JsonModel(array('tests' => $test));
    }
    
    
    protected function getObjectManager()
    {
        if (!$this->_objectManager) {
            $this->_objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }

        return $this->_objectManager;
    }
}