<?php
/**
 * __PROJECT_NAME__
 *
 * Module.php
 *
 * @category    
 * @package     
 * @subpackage  
 * @copyright   Copyright (c) 2013 Secondred Newmedia GmbH (http://www.secondred.de)
 * @created     17.12.13, 12:59
 * @author      Christian Schultze <schultze@secondred.de>, HoppC
 * @author      $Id$
 */

namespace DoctrineTest;

// Add these import statements:
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;


class Module
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    // Add this method:
    public function getServiceConfig()
    {
        return array(
            'factories' => array()
        );
    }

}