<?php
namespace DoctrineTest;

return array(
    'controllers' => array(
        'invokables' => array(
            'DoctrineTest\Controller\DoctrineTest' => 'DoctrineTest\Controller\DoctrineTestController',
        ),
    ),

    'router' => array(
        'routes' => array(
            'doctrinetest' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/doctrine',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'DoctrineTest\Controller\DoctrineTest',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            'doctrinetest' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        )
    ),
);