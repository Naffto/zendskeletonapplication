<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ZendSkeletonModuleTest;
use PHPUnit_Framework_TestCase;

class SampleTest extends PHPUnit_Framework_TestCase
{

    public function testSample()
    {
        $this->assertEquals('Test', 'Test');
    }
}
