<?php
use Zend\Loader\StandardAutoloader;

chdir(dirname(__DIR__));
/*define('ZF2_PATH', realpath(__DIR__ . '/vendor/zendframework/zendframework/library/'));
putenv('ZF2_PATH='.realpath(__DIR__ . '/vendor/zendframework/zendframework/library/'));
$path = array(
    ZF2_PATH,
    get_include_path(),
);
set_include_path(implode(PATH_SEPARATOR, $path));*/

include 'init_autoloader.php';

$loader = new StandardAutoloader();
$loader->registerNamespace('ZendSkeletonModuleTest', realpath(__DIR__ . '/ZendSkeletonModuleTest'));
$loader->register();

Zend\Mvc\Application::init(include 'config/application.config.php');